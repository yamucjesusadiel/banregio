package mx.com.yamadiel.banregio.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mx.com.yamadiel.banregio.domain.CustomerUsesCases
import mx.com.yamadiel.banregio.models.FullCardInformation
import javax.inject.Inject

@HiltViewModel
class CardViewModel @Inject constructor(private val customerUsesCases: CustomerUsesCases) :
    ViewModel() {

    private val _cardInformation = MutableLiveData<FullCardInformation>()
    val cardInformation: LiveData<FullCardInformation> = _cardInformation

    fun getCardInformation() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _cardInformation.postValue(customerUsesCases.getCard())
            }
        }
    }
}