package mx.com.yamadiel.banregio.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import mx.com.yamadiel.banregio.R
import mx.com.yamadiel.banregio.viewmodel.CardViewModel
import androidx.activity.viewModels
import mx.com.yamadiel.banregio.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: CardViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setUpViewModels()
    }

    private fun setUpViewModels(){
        viewModel.cardInformation.observe(this){
            if (it != null){
                binding.cardName.text = it.card?.nameCard
                binding.nameBank.text = it.card?.bankName
                binding.dateEnd.text = it.card?.dateEnd

                val numbers = it.card?.numberCard?.split("-")?.toTypedArray()
                binding.numberCardOne.text = numbers?.get(0) ?: ""
                binding.numberCardTwo.text = numbers?.get(1) ?: ""
                binding.numberCardThree.text = numbers?.get(2) ?: ""
                binding.numberCardFour.text = numbers?.get(3) ?: ""

                binding.cvv.text = "CVV"
                binding.cvv.setOnClickListener {
                    binding.cvv.text = getRandomString()
                }
            }
        }
        viewModel.getCardInformation()

    }

    private fun getRandomString(length: Int = 3) : String {
        val charset = "0123456789"
        return (1..length)
            .map { charset.random() }
            .joinToString("")
    }

}