package mx.com.yamadiel.banregio.domain

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import mx.com.yamadiel.banregio.data.remote.CardRemoteRepository
import mx.com.yamadiel.banregio.data.remote.OperationResult
import mx.com.yamadiel.banregio.models.Card
import mx.com.yamadiel.banregio.models.FullCardInformation
import java.lang.IllegalArgumentException
import javax.inject.Inject

class CustomerUsesCases @Inject constructor(private val cardRemoteRepository: CardRemoteRepository){
    suspend fun getCard(): FullCardInformation {
        val fullCardInformation = FullCardInformation()
        return coroutineScope {
            val response1 = async(Dispatchers.IO) { cardRemoteRepository.getCard() }
            val response2 = async(Dispatchers.IO) { cardRemoteRepository.getDetailCard() }

            try {
                val res = response1.await()
                val res2 = response2.await()

                res.let {
                    Log.i("TAG", it.toString())
                    fullCardInformation.card = it as Card
                }
                //res2.let { fullCardInformation.choferes = it.data as List<Chofer> }
                fullCardInformation

            }catch (ex: IllegalArgumentException){
                throw IllegalArgumentException("Error: ${ex.localizedMessage}")
            }
        }
    }
}