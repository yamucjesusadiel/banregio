package mx.com.yamadiel.banregio.models

import com.google.gson.annotations.SerializedName

class DetailCard (
    @SerializedName("pkMovimientosID")
    val idDetail: String,

    @SerializedName("Descripcion")
    val description: String,

    @SerializedName("Fecha")
    val date: String,

    @SerializedName("Monto")
    val bill: String
)