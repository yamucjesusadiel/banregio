package mx.com.yamadiel.banregio.models

import com.google.gson.annotations.SerializedName

class Card (

    @SerializedName("pkTarjetaCreditoID")
    val idCard: Int,

    @SerializedName("Nombre_Banco")
    val bankName: String,

    @SerializedName("Numero_Tarjeta")
    val numberCard: String,

    @SerializedName("Titular_Tarjeta")
    val nameCard: String,

    @SerializedName("Fecha_Exp")
    val dateEnd: String,

    @SerializedName("CVV")
    val cvv: Int,

    @SerializedName("Monto")
    val amount: Int

)


