package mx.com.yamadiel.banregio.data.remote

import retrofit2.Response
import retrofit2.http.GET

interface ApiServices {
    @GET("tarjetacredito.php/1")
    suspend fun getCard(): Response<GetCardResponse>

    @GET("tarjetacredito-movimientos.php/2")
    suspend fun getDetailCard(): Response<GetCardDetailsResponse>
}