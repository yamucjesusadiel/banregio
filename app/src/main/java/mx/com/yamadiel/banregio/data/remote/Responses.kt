package mx.com.yamadiel.banregio.data.remote

import mx.com.yamadiel.banregio.models.Card
import mx.com.yamadiel.banregio.models.DetailCard

data class GetCardResponse(val card: Card)
data class GetCardDetailsResponse(val detailCard: List<DetailCard>)