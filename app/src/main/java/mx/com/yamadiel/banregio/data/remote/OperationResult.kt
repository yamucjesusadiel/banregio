package mx.com.yamadiel.banregio.data.remote

sealed class OperationResult {
    data class ErrorMessage (val message:String?) : OperationResult()
    data class SuccessObject <T> (val data: T?) : OperationResult()
    data class SuccessList<T> (val data: List<T>?): OperationResult()
}