package mx.com.yamadiel.banregio.data.remote

import android.util.Log
import javax.inject.Inject

class CardRemoteRepository @Inject constructor(val apiServices: ApiServices) {

    suspend fun getCard(): OperationResult {
        try {
            val response =  apiServices.getCard()
            response.let {
                return if (it.isSuccessful) {
                    Log.i("TAG", "Success " + it.body())
                    OperationResult.SuccessObject(it.body()?.card)
                } else {
                    OperationResult.ErrorMessage("Ocurrio un error al realizar la petición")
                }
            }

        } catch (ex: Exception) {
            return OperationResult.ErrorMessage(ex.localizedMessage)
        }
    }

    suspend fun getDetailCard(): OperationResult {
        try {
            val response = apiServices.getDetailCard()
            response.let {
                return if (it.isSuccessful) {
                    OperationResult.SuccessObject(it.body())
                } else {
                    OperationResult.ErrorMessage("Ocurrio un error al realizar la petición")
                }
            }

        } catch (ex: Exception) {
            return OperationResult.ErrorMessage(ex.localizedMessage)
        }
    }

}