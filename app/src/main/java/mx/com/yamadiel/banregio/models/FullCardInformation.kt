package mx.com.yamadiel.banregio.models

import com.google.gson.annotations.SerializedName

class FullCardInformation (
    @SerializedName("")
    var card: Card? = null,

    @SerializedName("")
    var detailCard: List<DetailCard>? = null
)